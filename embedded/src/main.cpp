#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <Servo.h>

#include <config.h>
#include <prox_reader.h>
#include <api.h>
#include <main.h>

#define DEBUG

uint32_t last_interrupt_time = 0;
volatile uint8_t led_status = 0;
unsigned int unclaimed_coins = 0;
volatile unsigned int new_coins = 0;
volatile int cashout = 0;

Servo coin_servo;

void ISR_CASHOUT ();
void ISR_COIN ();

void setup() {
  Serial.begin(115200);

  WiFi.begin(cfg_wifi_ssid, cfg_wifi_password);
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  Serial.print(WiFi.localIP());
  Serial.print(" on ");
  Serial.println(cfg_wifi_ssid);

  pinMode(PIN_COIN, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_COIN), ISR_COIN, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_CASHOUT), ISR_CASHOUT, RISING);

  setup_prox();

  coin_servo.attach(4);
  coin_servo.write(SERVO_CLOSED);
}


unsigned long cur_prox = 0;

void loop() {
  cur_prox = read_prox();

  if(cur_prox != 0){
    Serial.print("Prox: ");
    Serial.println(cur_prox);

    if(unclaimed_coins > 0) {
      Serial.print("Claiming ");
      Serial.print(unclaimed_coins);
      Serial.println(" coins.");

      if(api_depositCoins(cur_prox, unclaimed_coins)==0){
        unclaimed_coins = 0;
      }

    } else if (cashout == 1){
      Serial.println("Withdrawing Coin");
      api_withdrawCoin(cur_prox);

      cashout = 0;
    } else {
      Serial.println("Balance Check");
      api_balanceCheck(cur_prox);
    }
  }

  if(new_coins > 0) {
    Serial.print(new_coins);
    Serial.println(" new coin");

    unclaimed_coins += new_coins;
    new_coins = 0;
  }

  if(cashout == 1 && unclaimed_coins > 0){
    Serial.println("Cashout");
    api_ejectCoin();
    unclaimed_coins -= 1;

    Serial.print(unclaimed_coins);
    Serial.println(" coins remain unclaimed.");
    
    cashout = 0;
  }

  if(cashout == 1){
    Serial.println("No coins to cashout.");
    cashout = 0;
  }


  digitalWrite(LED_BUILTIN, led_status);
}




void ISR_COIN () {
  uint32_t interrupt_time = millis();

  if(interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
    led_status = !led_status;
      new_coins += 1;
  }

  last_interrupt_time = interrupt_time;
}

void ISR_CASHOUT () {
  uint32_t interrupt_time = millis();

  if(interrupt_time - last_interrupt_time > DEBOUNCE_DELAY) {
    if(digitalRead(PIN_CASHOUT)){
      cashout = 1;
    }
  }

  last_interrupt_time = interrupt_time;
}
