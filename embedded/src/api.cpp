#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <string>

#include <api.h>
#include <config.h>
#include <main.h>

int user_coins = 0;

int api_sendTransaction(long prox_code, int coins_num){
    if(WiFi.status() != WL_CONNECTED){
        Serial.println("API: Wifi not connected.");
        return -1;
    }

    // WiFiClient client;
    HTTPClient http;

    String url = String(cfg_api_host) + "/transactions?transaction[value]=" + String(coins_num) + "&transaction[prox_num]=" + String(prox_code);

    http.begin(url);
    // http.setAuthorization(cfg_api_user, cfg_api_pass);
    Serial.println(url);
    int httpCode = http.POST((uint8_t *)0, 0);
    String payload = http.getString();
    Serial.println(httpCode);
    Serial.println(payload);

    if(httpCode == 302){
        return 0;
    }

    return httpCode;
}

int api_depositCoins(long prox_code, int coins_deposited){
    Serial.println("API: Depositing Coin");
    if(api_sendTransaction(prox_code, coins_deposited) == 0){
        // user_coins += coins_deposited;
        Serial.print(coins_deposited);
        Serial.println(" coins deposited.");
        return 0;
    }
    Serial.println("Wrong User");
    return 1;
}
int api_withdrawCoin(long prox_code){
    Serial.println("API: Withdrawing Coin");
    if(api_sendTransaction(prox_code, -1) == 0){
        // if(user_coins > 0){
        //     user_coins -= 1;
        //     Serial.print(user_coins);
        //     Serial.println(" coins total.");
        api_ejectCoin();
        return 0;
        // } else {
        //     Serial.println("No balance to draw from.");
        //     return 2;
        // }
    }

    Serial.println("Wrong User");
    return 1;
}
int api_ejectCoin(){
    Serial.println("API: Ejecting Coin");
    coin_servo.write(SERVO_OPEN);
    delay(600);
    coin_servo.write(SERVO_CLOSED);

    return 0;
}

int api_balanceCheck(long prox_code) {
    if(WiFi.status() != WL_CONNECTED){
        Serial.println("API: Wifi not connected.");
        return -1;
    }

    // WiFiClient client;
    HTTPClient http;

    String url = String(cfg_api_host) + "/users/prox/" + String(prox_code) + ".json";

    http.begin(url);
    // http.setAuthorization(cfg_api_user, cfg_api_pass);
    Serial.println(url);
    int httpCode = http.GET();
    String payload = http.getString();
    Serial.println(httpCode);
    Serial.println(payload);

    balance_start = payload.indexOf("balance:");
    
    Serial.println(payload[payload.indexOf("balance:")]);


    if(httpCode == 302){
        return 0;
    }

    return httpCode;

    Serial.println("API: Balance");
    Serial.print(user_coins);
    Serial.println(" coins");
    return 0;
}
