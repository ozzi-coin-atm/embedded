#ifndef main_h
#define main_h

#include <Servo.h>

#define PIN_COIN 5
#define PIN_CASHOUT 0
#define DEBOUNCE_DELAY 100 // in ms


#define SERVO_CLOSED 18
#define SERVO_OPEN 100

extern Servo coin_servo;
#endif
