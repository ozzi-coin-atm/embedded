/* configuration file */
#ifndef CONFIG_H
#define CONFIG_H

extern const char* cfg_wifi_ssid;
extern const char* cfg_wifi_password;

extern const bool cfg_api_https;
extern const char* cfg_api_host;
extern const uint16_t cfg_api_port;
extern const char* cfg_api_user;
extern const char* cfg_api_pass;

#endif
