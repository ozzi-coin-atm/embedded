/**** Interface with coin ejector and the internet ****
 * 
 * Todo: return error values and do not eat the coins
*/

int api_depositCoins(long prox_code, int num_coins);
int api_withdrawCoin(long prox_code);
int api_ejectCoin();
int api_balanceCheck(long prox_code);
